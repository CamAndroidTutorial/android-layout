package com.bunhann.layouttutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnLinear, btnLinear2, btnRelativeLayout, btnAbsoluteLayout, btnTableLayout,
            btnGridLayout, btnFrameLayout, btnConstrainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLinear = findViewById(R.id.btnLinear);
        btnLinear2 = findViewById(R.id.btnLinear2);
        btnRelativeLayout = findViewById(R.id.btnRelativeLayout);
        btnAbsoluteLayout = findViewById(R.id.btnAbsoluteLayout);
        btnTableLayout = findViewById(R.id.btnTableLayout);
        btnGridLayout = findViewById(R.id.btnGridLayout);
        btnFrameLayout = findViewById(R.id.btnFrameLayout);
        btnConstrainLayout = findViewById(R.id.btnConstrainLayout);

        btnLinear.setOnClickListener(this);
        btnLinear2.setOnClickListener(this);
        btnConstrainLayout.setOnClickListener(this);
        btnTableLayout.setOnClickListener(this);
        btnRelativeLayout.setOnClickListener(this);
        btnAbsoluteLayout.setOnClickListener(this);
        btnGridLayout.setOnClickListener(this);
        btnFrameLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnLinear:
                i = new Intent(v.getContext(), LinearActivity.class);
                startActivity(i);
                return;
            case R.id.btnLinear2:
                i = new Intent(v.getContext(), Linear2Activity.class);
                startActivity(i);
                return;
            case R.id.btnConstrainLayout:
                i = new Intent(v.getContext(), ConstraintActivity.class);
                startActivity(i);
                return;
            case R.id.btnTableLayout:
                i = new Intent(v.getContext(), TableActivity.class);
                startActivity(i);
                return;
            case R.id.btnGridLayout:
                i = new Intent(v.getContext(), GridActivity.class);
                startActivity(i);
                return;
            case R.id.btnFrameLayout:
                i = new Intent(v.getContext(), FrameActivity.class);
                startActivity(i);
                return;
            case R.id.btnAbsoluteLayout:
                i = new Intent(v.getContext(), AbsoluteActivity.class);
                startActivity(i);
                return;
            case R.id.btnRelativeLayout:
                i = new Intent(v.getContext(), RelativeActivity.class);
                startActivity(i);
                return;
            default:
                return;
        }
    }
}
